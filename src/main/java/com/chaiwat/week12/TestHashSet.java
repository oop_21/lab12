package com.chaiwat.week12;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.plaf.synth.SynthSpinnerUI;


public class TestHashSet {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet();
        set.add("A1");
        set.add("A2");
        set.add("A3");

        printset(set);

        set.add("A1");
        printset(set);
        System.out.println(set.contains("A1"));
        set.remove("A3");
        System.out.println(set);
        set.add("A1");

        HashSet<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(2);
        set2.add(3);
        System.out.println(set2);
        set2.add(3);
        System.out.println(set2);
        set2.clear();
        System.out.println(set2);
        
        HashSet<String> set3 = new HashSet<>();
        set3.addAll(set);
        System.out.println(set3);
        
    }
    public static void printset(HashSet<String> set){
        Iterator<String> Iterator = set.iterator();
        while(Iterator.hasNext()){
            System.out.println(Iterator.next());
        }
        System.out.println();
    }
}
